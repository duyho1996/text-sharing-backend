<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register','API\UserController@register');
Route::get('pastes/hash/{hash}', 'API\PasteController@getByHash');
Route::get('pastes/random', 'API\PasteController@getRandom');
Route::get('pastes/statistics', 'API\PasteController@getStatistics');
Route::resource('pastes', 'API\PasteController');
Route::resource('code-languages', 'API\CodeLanguageController');
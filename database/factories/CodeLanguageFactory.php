<?php

use Faker\Generator as Faker;

$factory->define(\App\CodeLanguage::class, function (Faker $faker) {
	$key = substr(md5(uniqid(rand(), true)), 0, 5);
	return [
        'key' => $key,
		'name' => strtoupper($key)
    ];
});

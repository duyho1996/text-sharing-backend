<?php

use App\Paste;
use Faker\Generator as Faker;

$factory->define(Paste::class, function (Faker $faker) {
	$hash = substr(md5(uniqid(rand(), true)), 0, 5);

	return [
	    'hash' => $hash,
	    'code_language_id' => function() {
			return factory(App\CodeLanguage::class)->create()->id;
	    },
	    'content' => '<?php echo "123; ?>',
	    'user_id' => function() {
			return factory(App\User::class)->create()->id;
	    },
	    'username' => 'myname',
	    'parent_id' => 0
    ];
});

<?php

namespace Tests\Unit;

use App\CodeLanguage;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CodeLanguageTest extends TestCase
{
	use DatabaseMigrations;

    public function testCanCreateCodeLanguage()
    {
    	$codeLanguage = CodeLanguage::create([
    		'key' => 'php',
		    'name' => 'PHP'
	    ]);
        $this->assertNotEmpty($codeLanguage);
    }
}

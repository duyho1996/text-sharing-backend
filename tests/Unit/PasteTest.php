<?php

namespace Tests\Unit;

use App\CodeLanguage;
use App\Paste;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PasteTest extends TestCase
{
	use DatabaseMigrations;

    public function testCanCreateNewPaste()
    {
    	$codeLanguage = factory(CodeLanguage::class)->create();
    	$user = factory(User::class)->create();

    	$hash = substr(md5(uniqid(rand(), true)), 0, 5);
    	$paste = Paste::create([
    		'hash' => $hash,
		    'code_language_id' => $codeLanguage->id,
		    'content' => '<?php echo "123; ?>',
		    'user_id' => $user->id,
		    'username' => $user->email,
		    'parent_id' => 0
	    ]);
        $this->assertNotEmpty($paste);
    }

    public function testCanGetCodeLanguageOfPaste() {
    	$paste = factory(Paste::class)->create();
    	$this->assertNotEmpty($paste->code_language);
    }

	public function testCanGetParentOfPaste() {
		$paste = factory(Paste::class)->create();
		$childrenPaste = factory(Paste::class)->create();
		$childrenPaste->parent_id = $paste->id;
		$childrenPaste->save();

		$this->assertNotEmpty($childrenPaste->parent);
	}

	public function testCanGetChildrenOfPaste() {
		$paste = factory(Paste::class)->create();
		$childrenPaste = factory(Paste::class)->create();
		$childrenPaste->parent_id = $paste->id;
		$childrenPaste->save();

		$this->assertEquals($childrenPaste->id, $paste->children()[0]->id);
	}
}

<?php

namespace Tests\Feature\API;

use App\CodeLanguage;
use App\Paste;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PasteControllerTest extends TestCase {
	use DatabaseMigrations;

	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testCanStorePaste() {
		$codeLanguage = factory( CodeLanguage::class )->create();
		$data         = [
			'code_language_id' => $codeLanguage->id,
			'content'          => '### this is H3',
			'username'         => 'user1'
		];
		$response     = $this->post( '/api/pastes/', $data );

		$createdPaste = Paste::orderBy( 'id', 'desc' )->first();

		$response->assertStatus( 200 );
		$response->assertJson( $createdPaste->toArray() );
	}

	public function testCanNotStorePasteWithInvalidCodeLanguage() {
		$data     = [
			'content'  => '### this is H3',
			'username' => 'user1'
		];
		$response = $this->post( '/api/pastes/', $data );

		$expectedResult = [
			'errors' => [
				'code_language_id' => [ 'The code language id field is required.' ]
			]
		];

		$response->assertStatus( 400 );
		$response->assertJson( $expectedResult );
	}

	public function testCanNotStorePasteWithInvalidContent() {
		$codeLanguage = factory( CodeLanguage::class )->create();
		$data         = [
			'code_language_id' => $codeLanguage->id,
			'username'         => 'user1'
		];
		$response     = $this->post( '/api/pastes/', $data );

		$expectedResult = [
			'errors' => [
				'content' => [ 'The content field is required.' ]
			]
		];

		$response->assertStatus( 400 );
		$response->assertJson( $expectedResult );
	}

	public function testCanNotStorePasteWithInvalidUsername() {
		$codeLanguage = factory( CodeLanguage::class )->create();
		$data         = [
			'code_language_id' => $codeLanguage->id,
			'content'          => '### this is H3',
		];
		$response     = $this->post( '/api/pastes/', $data );

		$expectedResult = [
			'errors' => [
				'username' => [ 'The username field is required.' ]
			]
		];

		$response->assertStatus( 400 );
		$response->assertJson( $expectedResult );
	}

	public function testCanShowPaste() {
		$parentPaste      = factory( Paste::class )->create();
		$paste            = factory( Paste::class )->create();
		$paste->parent_id = $parentPaste->id;
		$paste->save();

		$response = $this->get( "/api/pastes/$paste->id" );

		$expectedResult                  = $paste->toArray();
		$expectedResult['user']          = $paste->user->toArray();
		$expectedResult['code_language'] = $paste->code_language->toArray();
		$expectedResult['children']      = $paste->children()->toArray();
		$expectedResult['parent']        = $paste->parent->toArray();

		$response->assertStatus( 200 );
		$response->assertJson( $expectedResult );
	}

	public function testCanNotShowInvalidPaste() {
		$response = $this->get( "/api/pastes/999" );

		$expectedResult = [
			'errors' => ['Paste not found']
		];

		$response->assertStatus( 400 );
		$response->assertJson( $expectedResult );
	}

	public function testCanShowPasteByHash() {
		$parentPaste      = factory( Paste::class )->create();
		$paste            = factory( Paste::class )->create();
		$paste->parent_id = $parentPaste->id;
		$paste->save();

		$response = $this->get( "/api/pastes/hash/$paste->hash" );

		$expectedResult                  = $paste->toArray();
		$expectedResult['user']          = $paste->user->toArray();
		$expectedResult['code_language'] = $paste->code_language->toArray();
		$expectedResult['children']      = $paste->children()->toArray();
		$expectedResult['parent']        = $paste->parent->toArray();

		$response->assertStatus( 200 );
		$response->assertJson( $expectedResult );
	}

	public function testCanNotShowInvalidPasteByHash() {
		$response = $this->get( "/api/pastes/hash/12" );

		$expectedResult = [
			'errors' => ['Paste not found']
		];

		$response->assertStatus( 400 );
		$response->assertJson( $expectedResult );
	}

	public function testCanNotGetRandomPasteWithoutAnyData() {
		$response = $this->get( "/api/pastes/random" );

		$expectedResult = [
			'errors' => ['Paste not found']
		];

		$response->assertStatus( 400 );
		$response->assertJson( $expectedResult );
	}

	public function testCanGetRandomPaste() {
		$paste            = factory( Paste::class )->create();

		$response = $this->get( "/api/pastes/random" );

		$expectedResult                  = $paste->toArray();
		$expectedResult['user']          = $paste->user->toArray();
		$expectedResult['code_language'] = $paste->code_language->toArray();
		$expectedResult['children']      = $paste->children()->toArray();
		$expectedResult['parent']        = $paste->parent;

		$response->assertStatus( 200 );
		$response->assertJson( $expectedResult );
	}

	public function testCanGetStatistic() {
		$codeLanguage1 = CodeLanguage::create([
			'key' => 'code1',
			'name' => 'Code 1'
		]);

		$codeLanguage2 = CodeLanguage::create([
			'key' => 'code2',
			'name' => 'Code 2'
		]);

		$codeLanguage3 = CodeLanguage::create([
			'key' => 'code3',
			'name' => 'Code 3'
		]);

		$paste1 = Paste::create([
			'hash' => 'hash1',
			'code_language_id' => $codeLanguage1->id,
			'content' => '### This is h3',
			'username' => 'user1'
		]);

		$paste2 = Paste::create([
			'hash' => 'hash2',
			'code_language_id' => $codeLanguage2->id,
			'content' => '### This is h3',
			'username' => 'user2'
		]);

		$paste3 = Paste::create([
			'hash' => 'hash3',
			'code_language_id' => $codeLanguage3->id,
			'content' => '### This is h3',
			'username' => 'user3'
		]);

		$paste4 = Paste::create([
			'hash' => 'hash4',
			'code_language_id' => $codeLanguage1->id,
			'content' => '### This is h3',
			'username' => 'user3'
		]);

		$response = $this->get( "/api/pastes/statistics" );
		$expectedResult = [
			'paste_stats' => [
				['name' => 'Code 1', 'count' => 2],
				['name' => 'Code 2', 'count' => 1],
				['name' => 'Code 3', 'count' => 1],
			],
			'user_stats' => [
				['username' => 'user3', 'count' => 2],
				['username' => 'user1', 'count' => 1],
				['username' => 'user2', 'count' => 1],
			]
		];

		$response->assertStatus(200);
		$response->assertJson($expectedResult);
	}
}

<?php

namespace Tests\Feature\API;

use App\CodeLanguage;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CodeLanguageControllerTest extends TestCase
{
	use DatabaseMigrations;

	public function testCanGetAll() {
		factory( CodeLanguage::class )->create();
		factory( CodeLanguage::class )->create();
		factory( CodeLanguage::class )->create();
		factory( CodeLanguage::class )->create();
		factory( CodeLanguage::class )->create();

		$expectedResult = CodeLanguage::all()->toArray();

		$response = $this->get( "/api/code-languages" );

		$response->assertStatus(200);
		$response->assertJson($expectedResult);

	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeLanguage extends Model
{
    protected $table = 'code_language';
    protected $fillable = ['id', 'key', 'name'];
}

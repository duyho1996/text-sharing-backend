<?php

namespace App\Http\Controllers\API;

use App\Paste;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PasteController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$data = $request->all( [ 'code_language_id', 'content', 'user_id', 'username', 'parent_id' ] );

		$validatedData = Validator::make( $data,
			[
				'code_language_id' => [ 'required', Rule::exists( 'code_language', 'id' ) ],
				'content'          => 'required',
				'username'         => 'required'
			] );

		if ( $validatedData->fails() ) {
			return response()->json( [
				'errors' => $validatedData->errors()
			], 400 );
		}
		$hash         = substr( md5( uniqid( rand(), true ) ), 0, 5 );
		$data['hash'] = $hash;
		if ( ! $paste = Paste::create( $data ) ) {
			return response()->json( [
				'errors' => 'Can not create new paste'
			], 400 );
		}

		return response()->json( $paste, 200 );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		if ( ! $paste = Paste::find( $id ) ) {
			return response()->json( [
				'errors' => [
					'Paste not found'
				]
			], 400 );
		}

		$paste['user']          = $paste->user;
		$paste['code_language'] = $paste->code_language;
		$paste['children']      = $paste->children();
		$paste['parent']        = $paste->parent;

		return response()->json( $paste, 200 );
	}

	public function getByHash( Request $request, $hash ) {
		if ( ! $paste = Paste::where( 'hash', $hash )->first() ) {
			return response()->json( [
				'errors' => [
					'Paste not found'
				]
			], 400 );
		}

		$paste['user']            = $paste->user;
		$paste['code_language']   = $paste->code_language;
		$paste['parent']          = $paste->parent;
		$paste['children']        = $paste->children();
		$paste['diff_human_time'] = $paste->created_at->diffForHumans();

		return response()->json( $paste, 200 );
	}

	public function getRandom() {
		if ( ! $paste = Paste::inRandomOrder()->first() ) {
			return response()->json( [
				'errors' => [
					'Paste not found'
				]
			], 400 );
		}

		$paste['user']            = $paste->user;
		$paste['code_language']   = $paste->code_language;
		$paste['parent']          = $paste->parent;
		$paste['children']        = $paste->children();
		$paste['diff_human_time'] = $paste->created_at->diffForHumans();

		return response()->json( $paste, 200 );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}

	public function getStatistics() {
		$pasteStats = DB::table( 'paste' )
		                ->select( DB::raw( "code_language.name, count(code_language_id) as count" ) )
		                ->join( 'code_language', 'paste.code_language_id', '=', 'code_language.id' )
		                ->groupBy( 'code_language.name' )
		                ->orderBy( 'count', 'desc' )
		                ->limit( 10 )
		                ->get();

		$userStats = DB::table( 'paste' )
		                ->select( DB::raw( "username, count(code_language_id) as count" ) )
		                ->join( 'code_language', 'paste.code_language_id', '=', 'code_language.id' )
		                ->groupBy( 'username' )
		                ->orderBy( 'count', 'desc' )
		                ->limit( 10 )
		                ->get();

		$data = [
			'paste_stats' => $pasteStats,
			'user_stats'  => $userStats
		];

		return response()->json( $data );
	}
}

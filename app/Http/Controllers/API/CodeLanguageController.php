<?php

namespace App\Http\Controllers\API;

use App\CodeLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CodeLanguageController extends Controller
{
    public function index() {
    	$languages = CodeLanguage::all()->toArray();
    	return response()->json($languages);
    }
}

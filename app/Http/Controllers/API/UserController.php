<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;

class UserController extends Controller
{
//	function create(Request $request)
//	{
//		/**
//		 * Get a validator for an incoming registration request.
//		 *
//		 * @param  array  $request
//		 * @return \Illuminate\Contracts\Validation\Validator
//		 */
//		$valid = validator($request->only('email', 'name', 'password','mobile'), [
//			'name' => 'required|string|max:255',
//			'email' => 'required|string|email|max:255|unique:users',
//			'password' => 'required|string|min:6'
//		]);
//
//		if ($valid->fails()) {
//			$jsonError=response()->json($valid->errors()->all(), 400);
//			return response()->json($jsonError);
//		}
//
//		$data = request()->only('email','name','password');
//
//		$user = User::create([
//			'name' => $data['name'],
//			'email' => $data['email'],
//			'password' => bcrypt($data['password']),
//		]);
//
//		// And created user until here.
//
//		$client = Client::where('password_client', 1)->first();
//
//		// Is this $request the same request? I mean Request $request? Then wouldn't it mess the other $request stuff? Also how did you pass it on the $request in $proxy? Wouldn't Request::create() just create a new thing?
//
//		$request->request->add([
//			'grant_type'    => 'password',
//			'client_id'     => $client->id,
//			'client_secret' => $client->secret,
//			'username'      => $data['email'],
//			'password'      => $data['password'],
//			'scope'         => null,
//		]);
//
//		// Fire off the internal request.
//		$token = Request::create(
//			'oauth/token',
//			'POST'
//		);
//		return \Route::dispatch($token);
//	}

	public function login(){
		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
			$user = Auth::user();
			$success['token'] =  $user->createToken('MyApp')-> accessToken;
			return response()->json(['success' => $success], $this-> successStatus);
		}
		else{
			return response()->json(['error'=>'Unauthorised'], 401);
		}
	}
	/**
	 * Register api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['error'=>$validator->errors()], 401);
		}
		$input = $request->all();
		$input['password'] = bcrypt($input['password']);
		$user = User::create($input);
		$success['token'] =  $user->createToken('MyApp')-> accessToken;
		$success['name'] =  $user->name;
		return response()->json(['success'=>$success], 200);
	}
	/**
	 * details api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function details()
	{
		$user = Auth::user();
		return response()->json(['success' => $user], $this->successStatus);
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paste extends Model
{
	protected $table = 'paste';
	protected $fillable = ['id', 'hash', 'code_language_id', 'content', 'user_id', 'username', 'parent_id'];

	public function code_language() {
		return $this->belongsTo(CodeLanguage::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function parent() {
		return $this->belongsTo(Paste::class);
	}

	public function children() {
		return Paste::where('parent_id', $this->id)->get();
	}
}
